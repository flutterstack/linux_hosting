# linux single project hosting

$ git clone https://suresh-kumara-gist@bitbucket.org/flutterstack/linux_hosting.git
$ cd linux_hosting/single-project/
$ docker-compose up -d --build

# To make docker compose down $ docker-compose down -v

git clone https://bitbucket.org/suresh-kumara-gist/blogger-suresh.git app/

# import database
$ docker exec -i lhsp-codebase-1 mysql -u{MYSQL_ROOT_PASSWORD} -p{MYSQL_PASSWORD} --da
tabase={database_name} < {sql_file_path}.sql


$ docker exec -it {container_name} /bin/bash
$ cd /app/{project_name}/
$ composer install

create server sertificate

crontab -e
@monthly  yes | {full_path of init-letsencrypt.sh file}/init-letsencrypt.sh

# docker inspect <continerid>  to get the db ip

username: {mysql-username}
password: {mysql_password}


# https://medium.com/@nh3500/how-to-create-self-assigned-ssl-for-local-docker-based-lamp-dev-environment-on-macos-sierra-ab606a27ba8a
# http://tarunlalwani.com/post/how-to-debug-nginx-reverse-proxy-issues-php-fpm-gunicorn-uwsgi/
# https://www.ryadel.com/en/nginx-reverse-proxy-cache-centos-7-linux/
# https://www.linode.com/docs/web-servers/nginx/enable-tls-on-nginx-for-https-connections/
# https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71
